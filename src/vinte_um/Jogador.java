package vinte_um;

import java.util.ArrayList;

public class Jogador {
	
	String nome = "";
	private int pontos = 0;
	private boolean querContinuarJogando = true;
	ArrayList<Carta> mao = new ArrayList<Carta>();

	Jogador(String nome){
		this.nome = nome;
	}
	
	public void adicionarCarta(Carta carta) {
		mao.add(carta);
		calcularPontos(carta.valor);
	}
	
	private void calcularPontos(int valorCarta){
		pontos += valorCarta;
	}
	
	public int getPontos(){
		return pontos;
	}

	public boolean isQuerContinuarJogando() {
		return querContinuarJogando;
	}

	public void setQuerContinuarJogando(boolean querContinuarJogando) {
		this.querContinuarJogando = querContinuarJogando;
	}
}
